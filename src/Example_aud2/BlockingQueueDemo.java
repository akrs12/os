package Examples;

import java.util.ArrayList;
import java.util.List;

public class BlockingQueueDemo {


    public static void main(String[] args) throws InterruptedException {
        BlockingSemaphoreQueue<String> sharedMemory = new BlockingSemaphoreQueue<>(10);
        List<Producer> producers = new ArrayList<>();
        List<Consumer> consumers = new ArrayList<>();

        for(int i = 0; i < 10; i++){
            Producer producer = new Producer("Producer "+i, sharedMemory);
            Consumer consumer = new Consumer("Consumer "+i, sharedMemory);

            producers.add(producer);
            consumers.add(consumer);
        }

        for(int i = 0; i<10; i++)
            consumers.get(i).start();

        for(Producer producer: producers)
            producer.start();

        for(int i = 0; i<10; i++)
            consumers.get(i).join(2000);
        for(Producer producer: producers)
            producer.join(2000);

        System.out.println("Successfully execution. ");
    }
}

class Producer extends Thread{
    String name;
    BlockingSemaphoreQueue<String> sharedMemory;

    public Producer(String name,BlockingSemaphoreQueue<String>  sharedMemory){
        this.name = name;
        this.sharedMemory = sharedMemory;
    }

    @Override
    public void run() {
        for(int i = 0; i<5; i++){
            System.out.println("Producer "+name+" is adding product "+i);
            try {
                sharedMemory.enqueue("Product "+i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


class Consumer extends Thread{
    String name;
    BlockingSemaphoreQueue<String> sharedMemory;


    public Consumer(String name,BlockingSemaphoreQueue<String> sharedMemory){
        this.name = name;
        this.sharedMemory = sharedMemory;
    }

    @Override
    public void run() {
        for(int i = 0; i<5;i++){
            System.out.println("Consumer "+name+" is trying to  buy Product " + i);
            try {
                String pom = sharedMemory.dequeue();
                System.out.println("Consumer "+name+" bought the " + pom);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
