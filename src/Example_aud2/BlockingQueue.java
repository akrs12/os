package Examples;

import java.util.ArrayList;
import java.util.List;

//P1 P2 P3
//P1: [A,B] P2: [C,D] P3: [F,G] -----> [A,B,C,D,F,G] (6) --> FULL
public class BlockingQueue<T> {
    List<T> products = new ArrayList<>();
    int capacity;

    public BlockingQueue(int capacity){
        this.capacity = capacity;
    }

    //adding product from given producer
    public synchronized void enqueue(T item) throws InterruptedException {
        while(products.size() == capacity){
            //monitor waiting (thread waiting) until a product is removed from the store
            wait();
        }

        products.add(item);

        notifyAll();

    }

    //removing product by given costumer
    public synchronized T dequeue() throws InterruptedException {
        T item = null;
        while(products.size() == 0){
            //monitor waiting until at least one product is added into the store
            wait();
        }
        item = products.remove(products.size() - 1);
        notifyAll();
        return item;
    }

}
